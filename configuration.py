from functions import load_dataframe, get_action_from_condition_coding, get_signal_from_condition_coding, estimate_continuum
from experiment import Experiment
from model import TrustCalibrationPOMDP, ControlModel, RandomModel
import numpy as np
import random
import dill as pickle

# DATA SPREADSHEET
SPREADSHEET_CONFIG = {
    "file_id": '1oVxemJoSiIuuYEi2BAVojY1cQSXwD_Xip29hCPiGcLQ',
    "data_sheet": "data",
    "conditions_sheet": "samples_conditions",
    "outcomes_sheet": "ps_outcomes",
    "data_labels": {
        "pid": "PID",
        "tau": "tau",
        "c_label_function": lambda interface, difficulty: "c_{0}_{1}".format(interface, difficulty),
        "dprime_label_function": lambda interface, difficulty: "d_{0}_{1}".format(interface, difficulty)
    },
}

# STIMULI
STIMULI_CONFIG = {
    "known": {
        "n_stimuli": 1000,
        "noise_chance": 0.5, # pneumonia
        "ai_accuracy": 0.5 # correct recommendation
    },
    "unknown": {
        "n_stimuli": 1000,
        "noise_chance": 0.5, # pneumonia
        "ai_accuracy": 0.6 # correct recommendation
    },
    "batch_size": {
        "known": 10,
        "unknown": 10 
    }
}

# CONTINUUM (for continuum estimation from human experiment data)
CONTINUUM_CONFIG = {
    "interval_range": [0.1, 1.5],
    "step": 0.1,
    "noise_chance": 0.5,
    "ai_accuracy": 0.6
}

# METHOD
METHOD_CONFIG = {
    "random_seed": 0,
    "interfaces": ["control", "disclaimer", "preference"],
    "control_interface_idx": 0,
    "trust_levels": ["very_low", "low", "moderate", "high", "very_high"],
    "trust_range": [0, 1],
    "c_levels": ["minimal", "very_low", "low", "moderately_low", "moderate", "moderately_high", "high", "very_high", "maximal"],
    "c_range": [-2.32, 2.32],
    "parsing_functions": {
        "get_action": get_action_from_condition_coding,
        "get_stimulus_class": get_signal_from_condition_coding
    },
    "min_buffer_size_for_estimates": 6,
    "n_folds": 10,                          
}

# MODEL
POMDP_OPTIONS = {
    "difficulty_parsing_function": lambda condition_coding: "overall",
    "difficulty_levels": {
        0: "overall"
    },
    "horizon": 1,
    "gamma": 1.,
    "reward_alpha": 1.,
    "reward_gamma": 1.,
}

def pomdp_factory(training_data, experiment_config, model_config=POMDP_OPTIONS):
    model = TrustCalibrationPOMDP({
        "experiment_config": experiment_config,
        "model_options": model_config
    })
    if TrustCalibrationPOMDP.model_priors is None:
        print("Estimating POMDP priors...")
        priors = model.estimate_priors(training_data)
        TrustCalibrationPOMDP.save_priors(priors)
    
    model.initialise_model()
    return model

def load_experiment(json_exp_file=None, spreadsheet_config=SPREADSHEET_CONFIG, stimuli_config=STIMULI_CONFIG, method_config=METHOD_CONFIG, continuum_config=CONTINUUM_CONFIG, verbose=True):
    if json_exp_file is not None:
        experiment = None
        with open(json_exp_file, "rb") as fhandle:
            experiment = pickle.load(fhandle)
        
        return experiment
    
    # loading dataframes
    df_data = load_dataframe(
        spreadsheet_config["file_id"],
        spreadsheet_config["data_sheet"]
    )
    df_conditions = load_dataframe(
        spreadsheet_config["file_id"],
        spreadsheet_config["conditions_sheet"]
    )
    df_outcomes = load_dataframe(
        spreadsheet_config["file_id"],
        spreadsheet_config["outcomes_sheet"]
    )

    config = {
        "data": {
            "ps_data": df_data,
            "conditions_data": df_conditions,
            "outcomes_data": df_outcomes
        },
        "data_labels": spreadsheet_config["data_labels"],
        "stimuli_config": {
            "known": stimuli_config["known"],
            "unknown": stimuli_config["unknown"],
            "difficulty_levels": {}
        },
        "random_seed": method_config["random_seed"],
        "interfaces": method_config["interfaces"],
        "control_interface_idx": method_config["control_interface_idx"],
        "trust_levels": method_config["trust_levels"],
        "trust_range": method_config["trust_range"],
        "c_levels": method_config["c_levels"],
        "c_range": method_config["c_range"],
        "parsing_functions": method_config["parsing_functions"],
        "min_buffer_size_for_estimates": method_config["min_buffer_size_for_estimates"],
        "n_folds": method_config["n_folds"],
        "batch_size": stimuli_config["batch_size"],
        "options": {
            "verbose": verbose,
        }
    }

    experiment = Experiment(config, {})

    print("Estimating parameters for the continuum...")
    np.random.seed(config["random_seed"])
    random.seed(config["random_seed"])
    outcome = estimate_continuum(experiment, verbose=False, **continuum_config)

    interval = outcome[0][0]
    ratio = outcome[1][0]

    print("Continuum interval: (-{0}, {0}), ratio (of hard samples) = {1}%".format(interval, ratio*100))

    cutoff = -interval + 2*interval*ratio # cutoff is -1 * cutoff for noise
    section_hard_sig = (-interval, cutoff)
    section_easy_sig = (cutoff, interval)
    section_hard_noise = (-cutoff, interval)
    section_easy_noise = (-interval, -cutoff)

    config["continuum"] = {
        "interval": interval,
        "ratio": ratio
    }

    config["stimuli_config"]["difficulty_levels"] = {
        0: {
            "label": "easy",
            "mapping_function": 
            lambda stimulus: True if (stimulus.is_signal_ai() and float(stimulus.get_standard_value()) > section_easy_sig[0] and float(stimulus.get_standard_value()) <= section_easy_sig[1]) or (not stimulus.is_signal_ai() and float(stimulus.get_standard_value()) >= section_easy_noise[0] and float(stimulus.get_standard_value()) < section_easy_noise[1]) else False,
        },
        1: {
            "label": "hard",
            "mapping_function": 
            lambda stimulus: True if (stimulus.is_signal_ai() and float(stimulus.get_standard_value()) > section_hard_sig[0] and float(stimulus.get_standard_value()) <= section_hard_sig[1]) or (not stimulus.is_signal_ai() and float(stimulus.get_standard_value()) >= section_hard_noise[0] and float(stimulus.get_standard_value()) < section_hard_noise[1]) else False
        }
    }
    
    models_config = {
        "pomdp": {
            "factory_function": lambda training_data: pomdp_factory(training_data, config)
        },
        "control": {
            "factory_function": lambda training_data: ControlModel({
                "experiment_config": config,
                "model_options": {}
            })
        },
        "random": {
            "factory_function": lambda training_data: RandomModel({
                "experiment_config": config,
                "model_options": {}
            })
        }
        
    }

    return Experiment(config, models_config)

    

    



"""
"known": {
                "n_stimuli": 2500,
                "noise_chance": 0.5, # pneumonia
                "ai_accuracy": 0.5 # correct recommendation
            },
            "unknown": {
                "n_stimuli": 2500,
                "noise_chance": 0.5, # pneumonia
                "ai_accuracy": 0.6 # correct recommendation
            },
            "difficulty_levels": {
                #0: {
                #    "label": "easy",
                #    "mapping_function": 
                #    lambda stimulus: True if (stimulus.is_signal_ai() and float(stimulus.get_standard_value()) >= -0.25 and float(stimulus.get_standard_value()) <= 0.4) or (not stimulus.is_signal_ai() and float(stimulus.get_standard_value()) >= -0.40 and float(stimulus.get_standard_value()) < 0.25) else False,
                #},
                #1: {
                #    "label": "hard",
                #    "mapping_function": 
                #    lambda stimulus: True if (stimulus.is_signal_ai() and float(stimulus.get_standard_value()) > -0.4 and float(stimulus.get_standard_value()) <= -0.25) or (not stimulus.is_signal_ai() and float(stimulus.get_standard_value()) > 0.25 and float(stimulus.get_standard_value()) <= 0.4) else False
                #}
                
                # OK, using accuracy
                #0: {
                #    "label": "easy",
                #    "mapping_function": 
                #    lambda stimulus: True if (stimulus.is_signal_ai() and float(stimulus.get_standard_value()) > -0.15 and float(stimulus.get_standard_value()) <= 0.4) or (not stimulus.is_signal_ai() and float(stimulus.get_standard_value()) >= -0.4 and float(stimulus.get_standard_value()) < 0.15) else False,
                #},
                #1: {
                #    "label": "hard",
                #    "mapping_function": 
                #    lambda stimulus: True if (stimulus.is_signal_ai() and float(stimulus.get_standard_value()) > -0.4 and float(stimulus.get_standard_value()) <= 0.15) or (not stimulus.is_signal_ai() and float(stimulus.get_standard_value()) >= 0.15 and float(stimulus.get_standard_value()) < 0.4) else False
                #}
                
                # using c, dprime
                0: {
                    "label": "easy",
                    "mapping_function": 
                    lambda stimulus: True if (stimulus.is_signal_ai() and float(stimulus.get_standard_value()) > -0.24 and float(stimulus.get_standard_value()) <= 0.4) or (not stimulus.is_signal_ai() and float(stimulus.get_standard_value()) >= -0.4 and float(stimulus.get_standard_value()) < 0.24) else False,
                },
                1: {
                    "label": "hard",
                    "mapping_function": 
                    lambda stimulus: True if (stimulus.is_signal_ai() and float(stimulus.get_standard_value()) > -0.4 and float(stimulus.get_standard_value()) <= 0.24) or (not stimulus.is_signal_ai() and float(stimulus.get_standard_value()) >= 0.24 and float(stimulus.get_standard_value()) < 0.4) else False
                }
            
            }
"""