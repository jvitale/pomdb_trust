from model import TrustCalibrationPOMDP
import numpy as np
from stimulus import Stimulus
from sklearn.model_selection import KFold
from sklearn.linear_model import LinearRegression
from scipy.stats import norm
from participant import Participant
from functions import estimate_c, process_batch_outcomes
import random
import dill as pickle
import pandas as pd
from scipy.stats import ttest_rel
import pingouin as pg

class Experiment:
    def __init__(self, experiment_config, models_config):
        self._config = experiment_config
        self._config["models"] = models_config
        self._results = None
    
    def delete_results(self):
        self._results = None
    
    def save(self, filename):
        with open(filename, "wb") as fhandle:
            pickle.dump(self, fhandle)
    
    def get_experiment_configuration(self):
        return self._config            

    def estimate_stimuli_continuum(self, training_set):
        results = {
            "avg_dprime": {},
            "avg_c": {}
        }
        avg_dprime = 0
        avg_c = 0
        for difficulty_item in self._config["stimuli_config"]["difficulty_levels"].values():
            label = difficulty_item["label"]
            dprime_label = self._config["data_labels"]["dprime_label_function"](self._config["interfaces"][self._config["control_interface_idx"]], label)
            c_label = self._config["data_labels"]["c_label_function"](self._config["interfaces"][self._config["control_interface_idx"]], label)
            ps_df = training_set["ps_data"]
            dprime_values = np.array(ps_df[dprime_label])
            c_values = np.array(ps_df[c_label])
            results["avg_dprime"][label] = np.mean(dprime_values)
            avg_dprime += results["avg_dprime"][label]/len(self._config["stimuli_config"]["difficulty_levels"])
            results["avg_c"][label] = np.mean(c_values)
            avg_c += results["avg_c"][label]/len(self._config["stimuli_config"]["difficulty_levels"])
        
        results["avg_dprime"] = avg_dprime
        results["avg_c"] = avg_c
        return results
    
    def generate_stimulus(self, is_known, noise_chance=None, ai_accuracy=None, difficulty_levels=None, hard_chance=0.5):
        if is_known:
            key = "known"
        else:
            key = "unknown"
        
        if noise_chance is None:
            noise_chance = self._config["stimuli_config"][key]["noise_chance"]
        
        if ai_accuracy is None:
            ai_accuracy = self._config["stimuli_config"][key]["ai_accuracy"]
        
        if difficulty_levels is None:
            difficulty_levels = self._config["stimuli_config"]["difficulty_levels"]
        
        if np.random.random() < noise_chance:
            gt = Stimulus.NOISE
        else:
            gt = Stimulus.SIGNAL
        if np.random.random() < ai_accuracy:
            recommendation = Stimulus.SIGNAL if gt == Stimulus.SIGNAL else Stimulus.NOISE
        else:
            recommendation = Stimulus.SIGNAL if gt == Stimulus.NOISE else Stimulus.NOISE
        
        if np.random.random() < hard_chance:
            difficulty = "hard"
        else:
            difficulty = "easy"
        
        stimulus = Stimulus(1, gt, recommendation, difficulty_levels, difficulty)
        
        return stimulus
    
    def generate_stimuli(self):
        known_stimuli =  self.generate_batch_stimuli("known")
        unknown_stimuli = self.generate_batch_stimuli("unknown")

        return {"known": known_stimuli, "unknown": unknown_stimuli}
    
    def generate_batch_stimuli(self, batch_type):
        batch_stimuli = []

        for i in range(0, self._config["stimuli_config"][batch_type]["n_stimuli"]):
            stimulus = self.generate_stimulus(batch_type == "known")
            batch_stimuli.append(stimulus)
        
        batch_stimuli = np.array(batch_stimuli, dtype=object)

        return batch_stimuli
    
    def generate_kfolds(self):
        ps_df = self._config["data"]["ps_data"]
        conditions_df = self._config["data"]["conditions_data"]
        outcomes_df = self._config["data"]["outcomes_data"]
        pid_label = self._config["data_labels"]["pid"]
        kf = KFold(n_splits=self._config["n_folds"])
        data_folds = {}
        i = 0
        for train_index, test_index in kf.split(ps_df.to_numpy()):
            pid_training = np.array(ps_df.iloc[train_index][pid_label])
            pid_test = np.array(ps_df.iloc[test_index][pid_label])
            data_folds[i] = {
                'training': {
                    "ps_data": ps_df[ps_df[pid_label].isin(pid_training)],
                    "conditions_data": conditions_df[conditions_df[pid_label].isin(pid_training)],
                    "outcomes_data": outcomes_df[outcomes_df[pid_label].isin(pid_training)]
                },
                "test": {
                    "ps_data": ps_df[ps_df[pid_label].isin(pid_test)],
                    "conditions_data": conditions_df[conditions_df[pid_label].isin(pid_test)],
                    "outcomes_data": outcomes_df[outcomes_df[pid_label].isin(pid_test)]
                }
            }
            i+=1
        return data_folds
    
    def log(self, info):
        if self._config["options"]["verbose"]:
            print(info)

    def run_batch(self, ps, interface, batch_samples):
        outcomes = []
        for j in range(0, len(batch_samples)):
            stimulus = batch_samples[j]
            ps.new_stimulus(
                interface, # interface
                stimulus
            )
            outcome = ps.make_decision()
            outcomes.append(outcome)
            
        return outcomes
    
    def run(self):
        np.random.seed(self._config["random_seed"])
        random.seed(self._config["random_seed"])
        folds = self.generate_kfolds()
        samples_to_classify = self.generate_batch_stimuli("unknown")
        training_samples = self.generate_batch_stimuli("known")
        test_batch_size = self._config["batch_size"]["unknown"]
        test_batches = np.split(samples_to_classify, np.arange(test_batch_size, len(samples_to_classify), test_batch_size))
        training_batch_size = self._config["batch_size"]["known"]
        training_batches = np.split(training_samples, np.arange(training_batch_size, len(training_samples), training_batch_size))
        conditions = []
        for model_id in self._config["models"].keys():
            conditions.append(model_id)
        results = {
            "samples_to_classify": samples_to_classify,
            "test_batches": test_batches,
            "training_batches": training_batches,
            "conditions": conditions,
            "population": []
        }

        for k in range(0, len(folds)):
            self.log("Running experiment for fold {0}...".format(k+1))
            TrustCalibrationPOMDP.delete_priors()
            ps_df = folds[k]["test"]["ps_data"]
            fold_population = []
            global POMDP_PRIORS,  POMDP_BY_DIFFICULTY_PRIORS
            POMDP_PRIORS = None
            POMDP_BY_DIFFICULTY_PRIORS = None
            for i in range(0, len(ps_df)):
                cur_part = Participant(
                    ps_df.iloc[i],
                    self._config["data_labels"]["dprime_label_function"],
                    self._config["data_labels"]["c_label_function"]
                )
                for model_id, model_item in self._config["models"].items():
                    model_factory = model_item["factory_function"]
                    model = model_factory(folds[k]["training"])
                    cur_part.add_model(model_id, model)
                fold_population.append(cur_part)
                results["population"].append(cur_part)
                
            for i in range(0, len(test_batches)):
                unknown_samples = test_batches[i]
                known_samples = training_batches[i]

                for ps in fold_population:
                    pid = ps.get_data(self._config["data_labels"]["pid"])
                    for model_id in self._config["models"].keys():
                        model = ps.get_model(model_id)
                        interface = model.predict_best_interface()
                        test_batch_outcomes = self.run_batch(ps, interface, unknown_samples)
                        training_batch_outcomes = self.run_batch(ps, interface, known_samples)
                        processed_training_outcomes = process_batch_outcomes(training_batch_outcomes)
                        training_outcomes = {
                            "interface": interface,
                            "c": processed_training_outcomes["ai_results"]["c"],
                            "accuracy": processed_training_outcomes["task_results"]["accuracy"]
                        }
                        ps.record_batch_outcomes("batch{0}".format(i), model_id, True, training_batch_outcomes)
                        ps.record_batch_outcomes("batch{0}".format(i), model_id, False, test_batch_outcomes)
                        model.update_model_params(training_outcomes)
        
        self._results = results
    
    def get_average_task_accuracy_by_batch(self):
        assert self._results is not None, "Before plotting you need to run the experiment"

        accuracies = {}
        for condition in self._results["conditions"]:
            accuracies[condition] = []

        for condition in self._results["conditions"]:
            for ps in self._results["population"]:
                ps_accuracies = ps.get_accuracies_by_batch(condition)
                accuracies[condition].append(ps_accuracies["task_accuracy"])
            
        for condition in self._results["conditions"]:
            acc = np.array(accuracies[condition])
            acc = np.mean(acc, axis=0)
            accuracies[condition] = acc

        return accuracies
    
    def get_average_cumulative_task_accuracies_by_batch(self):
        assert self._results is not None, "Before plotting you need to run the experiment"

        accuracies = {}
        for condition in self._results["conditions"]:
            accuracies[condition] = []

        for condition in self._results["conditions"]:
            for ps in self._results["population"]:
                ps_accuracies = ps.get_cumulative_accuracies(condition)
                accuracies[condition].append(ps_accuracies["task_accuracy"])
            
        for condition in self._results["conditions"]:
            acc = np.array(accuracies[condition])
            acc = np.mean(acc, axis=0)
            accuracies[condition] = acc

        return accuracies
    
    def get_participants_final_task_accuracies(self):
        assert self._results is not None, "Before plotting you need to run the experiment"

        ps_accuracies = {}
        for condition in self._results["conditions"]:
            ps_accuracies[condition] = []
        n_test_batches = len(self._results["test_batches"])

        for condition in self._results["conditions"]:
            for ps in self._results["population"]:
                acc = ps.get_current_accuracy(condition)
                ps_accuracies[condition].append(acc["task_accuracy"])

        return ps_accuracies
    
    def generate_report(self):
        assert self._results is not None, "Before generating a report you need to run the experiment"

        r = self.get_average_task_accuracy_by_batch()

        r_dict = {}
        r_dict["batch"] = np.arange(1, len(r["control"])+1).tolist()
        for model in r.keys():
            r_dict[model] = r[model].tolist()

        avg_acc_by_batch_df = pd.DataFrame(r_dict)

        r = self.get_average_cumulative_task_accuracies_by_batch()

        r_dict = {}
        r_dict["batch"] = np.arange(1, len(r["control"])+1).tolist()
        for model in r.keys():
            r_dict[model] = r[model].tolist()

        cumulative_acc_by_batch_df = pd.DataFrame(r_dict)

        r = self.get_participants_final_task_accuracies()
        participants = []
        conditions = []
        accuracies = []
        for condition in r.keys():
            for pid, accuracy in enumerate(r[condition]):
                participants.append(pid)
                conditions.append(condition)
                accuracies.append(accuracy)
        
        ps_final_accuracy = pd.DataFrame(
            {
                'pid': np.array(participants),
                'model': np.array(conditions),
                'accuracy': np.array(accuracies)
            }
        )

        return {
            "average_accuracy_by_batch": avg_acc_by_batch_df,
            "cumulative_accuracy_by_batch": cumulative_acc_by_batch_df,
            "participants_final_accuracies": ps_final_accuracy
        }
    
    def analyse_report(self, report_df, column_target, alpha=0.05):
        if "batch" in report_df:
            # using a paired ttest
            columns = list(report_df.head())
            n_tests = len(columns) -2 # 2 being: "batch" and column_target
            alpha = alpha / n_tests #bonferroni correction

            is_corrected = n_tests > 1

            model_data = list(report_df[column_target])
            report = []
            for model in columns:
                if model in ["batch", column_target]:
                    continue
                
                cur_data = list(report_df[model])
                ttest_output = ttest_rel(model_data, cur_data)
                mean_diff = np.mean(np.array(model_data) - np.array(cur_data))
                is_significant = False
                if ttest_output[1] <= alpha:
                    is_significant = True
                
                result = [
                    "Paired ttest for '{0}' against '{1}'".format(column_target, model),
                    "alpha = {0} {1}".format(alpha, "(corrected with Bonferroni)" if is_corrected else ""),
                    "{0}".format(ttest_output),
                    "Mean difference = {0}".format(mean_diff),
                    "Difference between models is {0} significant".format("NOT" if not is_significant else "")
                ]
                report.append("\n".join(result))
                report.append("\n")
            
            return "\n".join(report)
        elif "pid" in report_df:
            # using repeated measures anova
            report = []
            anova_res = pg.rm_anova(dv=column_target, within="model", subject="pid", data=report_df, detailed=True)
            is_significant = False
            if "p-GG-corr" in anova_res and anova_res["p-GG-corr"].iloc[0] <= alpha:
                is_significant = True
            
            result = [
                "Repeated measures ANOVA for DV {0}".format(column_target),
                str(anova_res),
                "Difference within models is {0} significant".format("NOT" if not is_significant else "")
            ]

            report.append("\n".join(result))
            report.append("\n")

            result = []
            if is_significant:
                #posthoc
                posthocs = pg.pairwise_ttests(dv=column_target, within="model", subject="pid", padjust="bonf", data=report_df)

                result = [
                    "Posthoc analysis with Bonferroni correction",
                    str(posthocs)
                ]

                for i in range(0, len(posthocs)):
                    model_a = posthocs["A"].iloc[i]
                    model_b = posthocs["B"].iloc[i]
                    is_significant = posthocs["p-corr"].iloc[i] <= alpha
                    sign = ">" if posthocs["T"].iloc[i] > 0 else "<"
                    if is_significant:
                        sign_text = "({0} {1} {2})".format(model_a, sign, model_b)
                    else:
                        sign_text = ""
                    result.append(
                        "Difference between '{0}' and '{1}' is {2} significant {3}".format(model_a, model_b, "NOT" if not is_significant else "", sign_text)
                    )
            
            report.append("\n".join(result))
            report.append("\n")

            return "\n".join(report)
        
        return None

        