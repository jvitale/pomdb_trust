from scipy.stats import norm
import sys
from stimulus import Stimulus
import pandas as pd
import requests
from io import StringIO
import numpy as np
import participant
from scipy.optimize import least_squares

def load_dataframe(file_id, sheet_name):
    dwn_url='https://docs.google.com/spreadsheets/d/{0}/gviz/tq?tqx=out:csv&sheet={1}'.format(file_id, sheet_name)
    url = requests.get(dwn_url).text
    csv_raw = StringIO(url)
    return pd.read_csv(csv_raw)

def estimate_c(hit_rate, fa_rate):
    return -(norm.ppf(hit_rate) + norm.ppf(fa_rate)) / 2

def estimate_dprime(hit_rate, fa_rate):
    return norm.ppf(hit_rate) - norm.ppf(fa_rate)

def discretise(score, range, n_levels):
    norm_score = (score - range[0]) / (range[1] - range[0])
    return min(n_levels-1, int(max(0, norm_score - sys.float_info.epsilon) / (1./n_levels)))

def process_batch_outcomes(batch_outcomes, partial_results=None):
    if partial_results is None:
        ai_results = {
            "n_signal": 0,
            "n_noise": 0,
            "hit": 0,
            "correct_rejection": 0,
            "miss": 0,
            "false_alarm": 0,
            "accuracy": None,
            "c": None,
            "dprime": None
        }
        task_results = {
            "n_signal": 0,
            "n_noise": 0,
            "hit": 0,
            "correct_rejection": 0,
            "miss": 0,
            "false_alarm": 0,
            "accuracy": None
        }
    else:
        ai_results = partial_results["ai_results"]
        task_results = partial_results["task_results"]
    for outcome in batch_outcomes:
        if outcome["stimulus"].is_signal_ai():
            ai_results["n_signal"] += 1
        else:
            ai_results["n_noise"] += 1

        if outcome["stimulus"].is_signal_task():
            task_results["n_signal"] += 1
        else:
            task_results["n_noise"] += 1

        ai_results[outcome["ai_outcome"]] += 1
        task_results[outcome["task_outcome"]] += 1
    task_results["accuracy"] = (task_results["hit"] + task_results["correct_rejection"]) / (task_results["n_noise"] + task_results["n_signal"])
    ai_results["accuracy"] = (ai_results["hit"] + ai_results["correct_rejection"]) / (ai_results["n_noise"] + ai_results["n_signal"])
    hit_rate_ai = (ai_results["hit"] + 0.5)/ (ai_results["n_signal"] + 1)
    fa_rate_ai = (ai_results["false_alarm"] + 0.5)/ (ai_results["n_noise"] + 1)
    ai_results["c"] = estimate_c(hit_rate_ai, fa_rate_ai)
    ai_results["dprime"] = estimate_dprime(hit_rate_ai, fa_rate_ai)
    return {
        "task_results": task_results,
        "ai_results": ai_results
    }

def get_action_from_condition_coding(condition_coding):
    interface = condition_coding.split("_")[0]
    if interface == "disc":
        return "disclaimer"
    elif interface == "cont":
        return "control"
    elif interface == "pref":
        return "preference"
    else:
        raise Exception("Unexpected condition: {0}".format(interface))

def get_signal_from_condition_coding(condition_coding):
    stimulus = condition_coding.split("_")[3]
    if stimulus == "corr":
        return Stimulus.SIGNAL
    elif stimulus == "inco":
        return Stimulus.NOISE
    else:
        raise Exception("Unexpected stimulus condition: {0}".format(stimulus))

def estimate_continuum(experiment, interval_range=[0.1, 1], step=0.1, noise_chance=0.5, ai_accuracy=0.6, verbose=True):
    ps_df = experiment._config["data"]["ps_data"]
    def mapping(stimulus, difficulty_label, interval_signal, interval_noise, ratio_signal, ratio_noise):
        if stimulus.is_signal_ai():
            interval = interval_signal
            ratio = ratio_signal # ratio of hard samples within considered region
            cutoff = -interval + 2*interval*ratio
            if difficulty_label == "easy":
                region = (cutoff, interval)
            else:
                region = (-interval, cutoff)
        else:
            interval = interval_noise
            ratio = ratio_noise # ratio of hard samples within considered region
            cutoff = interval - 2*interval*ratio
            if difficulty_label == "easy":
                region = (-interval, cutoff)
            else:
                region = (cutoff, interval)
        
        return stimulus.get_standard_value() >= region[0] and stimulus.get_standard_value() < region[1]

    def to_minimise(x):
        stimuli_easy = []
        stimuli_hard = []
        difficulty_levels = {
            0: {
                "label": "easy",
                "mapping_function": lambda stimulus, d="easy", isig=x[0], inoise=x[1], rsig=x[2], rnoise=x[3]: mapping(stimulus, d, isig, inoise, rsig, rnoise)
            },
            1: {
                "label": "hard",
                "mapping_function": lambda stimulus, d="hard", isig=x[0], inoise=x[1], rsig=x[2], rnoise=x[3]: mapping(stimulus, d, isig, inoise, rsig, rnoise)
            }
        }

        for i in range(0, 500):
            stimulus = experiment.generate_stimulus(False, noise_chance, ai_accuracy, difficulty_levels)
            if stimulus.get_difficulty()["label"] == "easy":
                stimuli_easy.append(stimulus)
            else:
                stimuli_hard.append(stimulus)
        stimuli_easy = np.array(stimuli_easy, dtype=object)
        stimuli_hard = np.array(stimuli_hard, dtype=object)
        avg_MSE = 0
        for idx in range(0, len(ps_df)):
            cur_part = participant.Participant(
                ps_df.iloc[idx],
                experiment._config["data_labels"]["dprime_label_function"],
                experiment._config["data_labels"]["c_label_function"]
            )
            outcomes_easy = experiment.run_batch(cur_part, "control", stimuli_easy)
            processed_outcomes_easy = process_batch_outcomes(outcomes_easy)
            outcomes_hard = experiment.run_batch(cur_part, "control", stimuli_hard)
            processed_outcomes_hard = process_batch_outcomes(outcomes_hard)
            c_easy = processed_outcomes_easy["ai_results"]["c"]
            c_hard = processed_outcomes_hard["ai_results"]["c"]
            dprime_easy = processed_outcomes_easy["ai_results"]["dprime"]
            dprime_hard = processed_outcomes_hard["ai_results"]["dprime"]
            simulation_vector = np.array([c_easy, c_hard, dprime_easy, dprime_hard])
            gt_c_easy = cur_part.get_data("c_control_easy")
            gt_c_hard = cur_part.get_data("c_control_hard")
            gt_dprime_easy = cur_part.get_data("d_control_easy")
            gt_dprime_hard = cur_part.get_data("d_control_hard")
            gt_vector = np.array([gt_c_easy, gt_c_hard, gt_dprime_easy, gt_dprime_hard])

            #avg_MSE += simulation_vector - gt_vector
            MSE = np.linalg.norm(simulation_vector - gt_vector)
            avg_MSE += MSE / len(ps_df)
        
        if verbose:
            print("Call with parameter {0} got result = {1}".format(x, avg_MSE))
        return avg_MSE

    steps = np.arange(interval_range[0], interval_range[1], step) # considered intervals
    ratios = np.arange(0.1, 1, step)
    min_cost = None
    min_intervals = None
    min_ratios = None
    for interval_signal in steps:
        interval_noise = interval_signal
        for ratio_signal in ratios:
            ratio_noise = ratio_signal
            cur_cost = to_minimise([interval_signal, interval_noise, ratio_signal, ratio_noise])
            if min_cost is None or cur_cost < min_cost:
                min_cost = cur_cost
                min_intervals = [interval_signal, interval_noise]
                min_ratios = [ratio_signal, ratio_noise]
    
    return min_intervals, min_ratios
    