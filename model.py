from abc import ABC, abstractmethod
import numpy as np
from sklearn.linear_model import LinearRegression
from stimulus import Stimulus
from pomdp import POMDPModel
from functions import estimate_c, discretise
import random

class Model(ABC):

    def __init__(self, model_args):
        self._model_args = model_args
        self._model = None
        self._history_vars = {}
    
    def record_history_var(self, varname, value):
        self._history_vars[varname] = value
    
    def get_history_var(self, varname):
        if varname in self._history_vars:
            return self._history_vars[varname]
        else:
            return None
    
    @abstractmethod
    def initialise_model(self, model_params):
        pass
    
    @abstractmethod
    def predict_best_interface(self):
        pass
    
    @abstractmethod
    def update_model_params(self, training_batch_outcomes):
        pass

class TrustCalibrationPOMDP(Model):

    model_priors = None

    def __init__(self, model_args):
        super().__init__(model_args)
        # todo check that there all the necessary options
        assert "experiment_config" in model_args, "The parameter model_args must have a key 'experiment_config' with the configuration of the experiment."
        assert "model_options" in model_args, "The parameter model_args must have a key 'model_options' with the options of the model."
        self._config = model_args["experiment_config"]
        self._options = model_args["model_options"]
        self._regressions = None
    
    def save_priors(priors):
        TrustCalibrationPOMDP.model_priors = priors
    
    def delete_priors():
        TrustCalibrationPOMDP.model_priors = None
    
    def linear_regression(self, training_data):
        assert "ps_data" in training_data, "The parameter training_data must have a key 'ps_data'."

        tau_label = self._config["data_labels"]["tau"]
        sampled_taus = np.array(training_data["ps_data"][tau_label])
        sampled_taus = np.reshape(sampled_taus, (sampled_taus.size,1))
        regressions = {}
        for difficulty_level, label in self._options["difficulty_levels"].items():
            c_label = self._config["data_labels"]["c_label_function"](self._config["interfaces"][self._config["control_interface_idx"]], label)
            sampled_criteria = np.array(training_data["ps_data"][c_label])
            norm_criteria = (sampled_criteria - self._config["c_range"][0]) / (self._config["c_range"][1] - self._config["c_range"][0])
            reg = LinearRegression().fit(sampled_taus, norm_criteria)
            regressions[difficulty_level] = reg

        return regressions
    
    def get_c_level(self, c_score):
        c_range = self._config["c_range"]
        n_levels = len(self._config["c_levels"])
        c_level = discretise(c_score, c_range, n_levels)
        return c_level
    
    def estimate_tau_from_regression(self, c, regression):
        coeff = regression.coef_[0]
        norm_c = (c - self._config["c_range"][0]) / (self._config["c_range"][1] - self._config["c_range"][0])
        intercept = regression.intercept_
        tau_score = (norm_c - intercept) / coeff
        return max(self._config["trust_range"][0], min(self._config["trust_range"][1], tau_score))
    
    def get_tau_level(self, tau_score):
        tau_range = self._config["trust_range"]
        n_levels = len(self._config["trust_levels"])
        tau_level = discretise(tau_score, tau_range, n_levels)
        return tau_level
    
    def get_difficulty_level(self, difficulty_label):
        key_list = list(self._options["difficulty_levels"].keys())
        val_list = list(self._options["difficulty_levels"].values())
        idx = val_list.index(difficulty_label)
        return key_list[idx]
    
    def get_ps_states(self, pid, regressions, grouped_by_interface=False):       
        df_conditions = self._config["data"]["conditions_data"]
        df_outcomes = self._config["data"]["outcomes_data"]
        cur_ps_conditions = np.array(df_conditions[df_conditions["PID"] == pid])[0]
        cur_ps_outcomes = np.array(df_outcomes[df_conditions["PID"] == pid])[0]
        counts = {}
        if grouped_by_interface:
            for interface in self._config["interfaces"]:
                counts[interface] = {
                    "n_signal": 1,
                    "n_noise": 1,
                    "n_hits": 0.5,
                    "n_fa": 0.5
                }
        else:
            counts["overall"] = {
                "n_signal": 1,
                "n_noise": 1,
                "n_hits": 0.5,
                "n_fa": 0.5
            }
        ps_states = []
        for j in range(1, len(cur_ps_conditions)-1):
            cur_coding = cur_ps_conditions[j]
            action = self._config["parsing_functions"]["get_action"](cur_coding)
            counts_group = action if grouped_by_interface else "overall"
            difficulty = self._options["difficulty_parsing_function"](cur_coding)

            difficulty_level = self.get_difficulty_level(difficulty)
            if difficulty_level is None:
                raise(Exception("There is something wrong with conditions data coding {0}. Difficulty level for {1} missing".format(cur_coding, difficulty)))
            stimulus_ai_class = self._config["parsing_functions"]["get_stimulus_class"](cur_coding)
            outcome = cur_ps_outcomes[j]

            if stimulus_ai_class == Stimulus.SIGNAL:
                counts[counts_group]["n_signal"] += 1
            else:
                counts[counts_group]["n_noise"] += 1

            if outcome.lower() == "hit":
                counts[counts_group]["n_hits"] += 1
            elif outcome.lower() == "fa":
                counts[counts_group]["n_fa"] += 1
            
            
            c = None
            tau = None
            tau_level = None
            if counts[counts_group]["n_signal"] + counts[counts_group]["n_noise"] >= self._config["min_buffer_size_for_estimates"]+2:
                hit_rate = counts[counts_group]["n_hits"] / counts[counts_group]["n_signal"]
                fa_rate = counts[counts_group]["n_fa"] / counts[counts_group]["n_noise"]
                c = estimate_c(hit_rate, fa_rate)
                tau = self.estimate_tau_from_regression(c, regressions[difficulty_level])
                tau_level = self.get_tau_level(tau)

            ps_states.append({
                "action": action,
                "difficulty": difficulty,
                "difficulty_level": difficulty_level,
                "c": c,
                "tau": tau,
                "tau_level": tau_level,
                "outcome": outcome.lower()
            })
        return ps_states
    
    def estimate_priors(self, training_data):
        regressions = self.linear_regression(training_data)
        pid_label = self._config["data_labels"]["pid"]
        S = {}
        n_trust_levels = len(self._config["trust_levels"])
        for idx_diff, label in self._options["difficulty_levels"].items():
            for idx, trust_level in enumerate(self._config["trust_levels"]):
                S[idx_diff*n_trust_levels + idx] = "{0}_{1}".format(trust_level, label)
        
        A = {}
        for idx, interface in enumerate(self._config["interfaces"]):
            A[idx] = interface
        
        OM = {}
        for idx, accuracy in enumerate(self._config["c_levels"]):
            OM[idx] = accuracy

        # reward values
        Rvalues = {
            0: (0., lambda delta_acc: abs(delta_acc) <= 0.01),
            1: (1., lambda delta_acc: delta_acc > 0.01 and delta_acc <= 0.05),
            2: (2., lambda delta_acc: delta_acc > 0.05 and delta_acc <= 0.1),
            3: (3., lambda delta_acc: delta_acc > 0.1),
            4: (-1., lambda delta_acc: delta_acc < -0.01 and delta_acc >= -0.05),
            5: (-2., lambda delta_acc: delta_acc < -0.05 and delta_acc >= -0.1),
            6: (-3., lambda delta_acc: delta_acc < -0.1)
        }
        
        
        T_counts = np.ones((len(S.keys()), len(S.keys()), len(A.keys()))) # to ones to avoid that not observing any event in a cell leads to null probability
        O_counts = np.ones((len(S.keys()), len(OM.keys()), len(A.keys()))) # to ones to avoid that not observing any event in a cell leads to null probability
        R_counts = np.ones((len(Rvalues.keys()), len(S.keys()), len(A.keys())))
        
        train_PID = np.array(training_data["ps_data"][pid_label])
        for pid in train_PID:
            states = self.get_ps_states(pid, regressions)
            for i, state in enumerate(states):
                difficulty_level = states[i]["difficulty_level"]
                if states[i]["tau"] is not None:
                    s_1 = difficulty_level*n_trust_levels + self.get_tau_level(states[i]["tau"])
                else:
                    s_1 = None
                action = list(A.values()).index(state["action"])
                if i > 0 and states[i-1]["tau"] is not None:
                    s_0 = difficulty_level*n_trust_levels + self.get_tau_level(states[i-1]["tau"])
                else:
                    s_0 = None
                
                if states[i]["c"] is not None:
                    o_1 = self.get_c_level(states[i]["c"])
                else:
                    o_1 = None
                
                if s_0 is not None and s_1 is not None:
                    T_counts[s_0, s_1, action] += 1
                if s_1 is not None and o_1 is not None:
                    O_counts[s_1, o_1, action] += 1
        
        return [regressions, (S, A, OM, T_counts, O_counts, R_counts, Rvalues)]
    
    def initialise_model(self, model_params=None):
        if model_params is None:
            model_params = TrustCalibrationPOMDP.model_priors
        self._regressions = model_params[0]
        self._model = POMDPModel(*model_params[1])
    
    def predict_best_interface(self):
        action_id =  self._model.next_action(
            horizon=self._options["horizon"],
            gamma=self._options["gamma"]
        )
        interface_label = self._model._A[action_id]
        
        return interface_label
    
    def update_model_params(self, training_batch_outcomes):
        interface = training_batch_outcomes["interface"]
        interface_idx = self._config["interfaces"].index(interface)
        computed_c = training_batch_outcomes["c"]
        task_accuracy = training_batch_outcomes["accuracy"]
        observation = self.get_c_level(computed_c)
        last_accuracy = self.get_history_var("last_accuracy")
        self.record_history_var("last_accuracy", task_accuracy)
        if last_accuracy is not None:
            delta_accuracy = task_accuracy - last_accuracy
            _, reward = self._model.get_reward_from_delta_accuracy(delta_accuracy)
        else:
            reward = None
        self._model.update(
            interface_idx,
            observation,
            reward, 
            reward_alpha=self._options["reward_alpha"],
            reward_gamma=self._options["reward_gamma"]
        )

class ControlModel(Model):

    def __init__(self, model_args):
        super().__init__(model_args)
    
    def initialise_model(self, model_params):
        pass
    
    def predict_best_interface(self):
        return "control"
    
    def update_model_params(self, training_batch_outcomes):
        pass

class RandomModel(Model):

    def __init__(self, model_args):
        super().__init__(model_args)
        self._config = model_args["experiment_config"]
    
    def initialise_model(self, model_params):
        pass
    
    def predict_best_interface(self):
        return random.choice(self._config["interfaces"])
    
    def update_model_params(self, training_batch_outcomes):
        pass