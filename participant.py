from stimulus import Stimulus
import functions 

class Participant:

    def __init__(self, ps_data, dprime_label_function, c_label_function):
        self._ps_data = ps_data
        self._label_functions = {
            "dprime": dprime_label_function,
            "c": c_label_function
        }
        self._current_stimulus = None
        self._current_interface = None
        self._current_difficulty = None
        self._current_c = None
        self._current_dprime = None
        self._models = {}
        self._batch_outcomes = {}
    
    #def init_pomdp_model(self, model_id, model_params):
    #    self._pomdp_model[model_id] = POMDPModel(*model_params)
    
    #def get_pomdp_model(self, model_id):
    #    return self._pomdp_model[model_id]

    def add_model(self, model_id, model):
        self._models[model_id] = model
    
    def get_model(self, model_id):
        if model_id in self._models:
            return self._models[model_id]
        else:
            return None
    
    def record_batch_outcomes(self, batch_id, exp_condition, is_training, outcomes):
        if exp_condition not in self._batch_outcomes:
            self._batch_outcomes[exp_condition] = {}
        
        if is_training:
            label = "training_samples"
        else:
            label = "test_samples"
        
        if label not in self._batch_outcomes[exp_condition]:
            self._batch_outcomes[exp_condition][label] = {}
        
        self._batch_outcomes[exp_condition][label][batch_id] = outcomes
    
    def get_batch_outcomes(self, batch_id, exp_condition, is_training):
        if is_training:
            label = "training_samples"
        else:
            label = "test_samples"
        return self._batch_outcomes[exp_condition][label][batch_id]

    def get_cumulative_accuracies(self, condition):
        partials = None
        accuracies = {
            "task_accuracy": [],
            "ai_accuracy": []
        }
        for batch_outcomes in self._batch_outcomes[condition]["test_samples"].values():
            if partials is None:
                batch_results = functions.process_batch_outcomes(batch_outcomes)
            else:
                batch_results = functions.process_batch_outcomes(batch_outcomes, partials)
            accuracies["task_accuracy"].append(batch_results["task_results"]["accuracy"])
            accuracies["ai_accuracy"].append(batch_results["ai_results"]["accuracy"])
            partials = batch_results

        return accuracies
    
    def get_accuracies_by_batch(self, condition):
        accuracies = {
            "task_accuracy": [],
            "ai_accuracy": []
        }
        for batch_outcomes in self._batch_outcomes[condition]["test_samples"].values():
            batch_results = functions.process_batch_outcomes(batch_outcomes)
            accuracies["task_accuracy"].append(batch_results["task_results"]["accuracy"])
            accuracies["ai_accuracy"].append(batch_results["ai_results"]["accuracy"])
        
        return accuracies

    def get_current_accuracy(self, condition):
        accuracies = self.get_cumulative_accuracies(condition)

        return {
            "task_accuracy": accuracies["task_accuracy"][-1],
            "ai_accuracy": accuracies["ai_accuracy"][-1],
        }
    
    


    
    #def record_history_var(self, varname, value):
    #    self._history_vars[varname] = value
    
    #def get_history_var(self, varname):
    #    if varname in self._history_vars:
    #        return self._history_vars[varname]
    #    else:
    #        return None
    
    def get_data(self, column_name):
        return getattr(self._ps_data, column_name)  
        
    def new_stimulus(self, current_interface, stimulus):
        self._current_interface = current_interface
        self._current_difficulty = stimulus.get_difficulty()
        current_c_label = self._label_functions["c"](self._current_interface, self._current_difficulty["label"])
        current_dprime_label = self._label_functions["dprime"](self._current_interface, self._current_difficulty["label"])
        c = float(getattr(self._ps_data, current_c_label))
        self._current_c = c
        d_prime = float(getattr(self._ps_data, current_dprime_label))
        self._current_dprime = d_prime
        self._current_stimulus = stimulus
        #difficulty = stimulus.get_difficulty()
        #new_dyn_trust = self._c_to_dyn_tau(c, difficulty["label"])
        #self._current_dynamic_trust = new_dyn_trust
    
    def make_decision(self):
        accept = True if self._current_stimulus.get_perceived_value(self._current_dprime) >= self._current_c else False
        if accept:
            choice = self._current_stimulus.get_recommendation()
        else:
            choice = Stimulus.SIGNAL if self._current_stimulus.get_recommendation() == Stimulus.NOISE else Stimulus.NOISE
        task_outcome = self._current_stimulus.get_task_outcome(choice)
        ai_outcome = self._current_stimulus.get_ai_outcome(choice)
        outcome = {
            "stimulus": self._current_stimulus,
            "choice": choice,
            "task_outcome": task_outcome,
            "ai_outcome": ai_outcome
        }
        return outcome
    
    def observe_c(self):
        return self._current_c
    
    #def observe_tau(self):
    #    return self._current_dynamic_trust
