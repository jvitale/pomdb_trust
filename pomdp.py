import numpy as np

class POMDPModel:
    def __init__(self, S, A, OM, T_counts, O_counts, R_counts, reward_values):
        self._S = S.copy()
        self._A = A.copy()
        self.n_states = len(self._S)
        self.n_actions = len(self._A)
        self._OM = OM.copy()
        self._T_counts = T_counts.copy()/T_counts.sum() # this is to average the counts over total samples
        self._O_counts = O_counts.copy()/O_counts.sum() # this is to average the counts over total samples
        self._R_counts = R_counts.copy()/R_counts.sum() # this is to average the counts over total samples
        self._Rvalues = reward_values.copy()
        self._T = T_counts.copy() / T_counts.sum(axis=1, keepdims=True)
        self._O = O_counts.copy() / O_counts.sum(axis=1, keepdims=True)
        self._R = R_counts.copy() / R_counts.sum(axis=1, keepdims=True)
        self._Q = np.zeros((self.n_states, self.n_actions))
        for state in range(0, self.n_states):
            for action in range(0, self.n_actions):
                self._Q[state, action] = self.reward_expected_value(state, action)
        
        self._belief_prior = np.ones((T_counts.shape[0],))/T_counts.shape[0] #uniform prior over states
    
    def get_reward_from_delta_accuracy(self, delta_acc):
        for idx, rv in enumerate(self._Rvalues.values()):
            output = rv[1](delta_acc)
            if output is True:
                return (idx, rv[0])

        # this should not happen
        raise(Exception('Something is not good with reward mapping function (delta = {0})'.format(delta_acc)))
    
    def reward_expected_value(self, state, action):
        return (self._R[:, state, action] * np.array([x[0] for x in self._Rvalues.values()])).sum()
    
    def pr_of_o_given_b_and_a(self, observation, action, belief=None):
        # P(o | b, a) with b current beliefs
        probability = 0.
        if belief is None:
            belief = self._belief_prior
        for s_prime, _ in enumerate(belief):
            factor_a = self._O[s_prime, observation, action]
            factor_b = (self._T[:, s_prime, action] * belief).sum()
            probability += factor_a * factor_b
            
        return probability
    
    def belief_update(self, action, obs, belief_prior=None):
        if belief_prior is None:
            b = self._belief_prior.copy()
            belief_prior = self._belief_prior.copy()
        else:
            b = belief_prior.copy()
        for s_prime, belief in enumerate(belief_prior):
            b[s_prime] = self._O[s_prime, obs, action] * (self._T[:, s_prime, action] * belief_prior).sum()
        
        updated_belief_state = b/b.sum() # this is the same as dividing for self.pr_of_o_given_b_and_a(obs, action)
        if belief_prior is None:
            # updating in model
            self._belief_prior = updated_belief_state
        else:
            return updated_belief_state
    
    def predicted_state(self):
        return np.argmax(self._belief_prior)
    
    def get_beliefs(self):
        return self._belief_prior
    
    def update_reward_prior(self, action, reward, alpha=0.25, gamma=0.5):
        # the beliefs were already updated after
        # getting the observation, now it is the
        # turn of updating Q
        for state in range(0, self.n_states):
            self._Q[state, action] += alpha * self._belief_prior[state] * (reward + (gamma * np.max(self._Q[state, :])) - self._Q[state, action])
    
    def value_function(self, belief, action, gamma=0.5, horizon=1):
        reward_factor = (self._Q[:, action] * belief).sum()
        future_factor = 0.
        if horizon > 1:
            for o in range(0, self._O.shape[1]):
                p_o_given_a_b = self.pr_of_o_given_b_and_a(o, action, belief)
                updated_b = self.belief_update(action, o, belief_prior=belief)
                max_a = None
                max_value = None
                for a in range(0, self.n_actions):
                    cur_value = self.value_function(updated_b, a, gamma, horizon-1)
                    if max_a is None or cur_value > max_value:
                        max_a = a
                        max_value = cur_value
                
                #next_value_function = self.value_function(updated_b, action, gamma, horizon-1)
                future_factor += p_o_given_a_b * max_value
        return reward_factor + gamma * future_factor
    
    def next_action(self, gamma=0.5, horizon=1):
        best_action = None
        V_max = None
        for action in range(0, self.n_actions):
            V_cur = self.value_function(self._belief_prior, action, gamma, horizon)
            #V_cur = (self._belief_prior * self._Q[:, action]).sum()
            if V_max is None or V_cur > V_max:
                V_max = V_cur
                best_action = action
        
        return best_action
    
    def update_transition_matrix(self, b_t0, action, gamma=0.5):
        # [T_counts | a] = [T_counts | a] + P(b_t0)P(b_t) with b_t0 previous belief state and b_t current belief state
        self._T_counts[:, :, action] += gamma * np.dot(np.reshape(b_t0, (self.n_states,1)), np.reshape(self._belief_prior, (self.n_states,1)).T)
        self._T = (self._T_counts/self._T_counts.sum(axis=1, keepdims=True)).copy()
    
    #def update_observation_matrix(self, action, observation):
    #    # [O_counts | a, o] = [O_counts | a, o] + P(b_t)
    #    self._O_counts[:, observation, action] += self._belief_prior
    #    self._O = (self._O_counts/self._O_counts.sum(axis=1, keepdims=True)).copy()
    
    def update(self, action, observation, reward, reward_alpha=0.25, reward_gamma=0.5):
        b_t0 = self.get_beliefs()
        self.belief_update(action, observation)
        if reward is not None:
            self.update_reward_prior(action, reward, reward_alpha, reward_gamma)
        self.update_transition_matrix(b_t0, action)
        #self.update_observation_matrix(action, observation)
    
    def plot_reward_function(self):
        Y = np.arange(0, self.n_states, 1)
        X = np.arange(0, self.n_actions, 1)
        X, Y = np.meshgrid(X, Y)

        return (X, Y, self._Q)
        
        """
        Code to plot:

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.plot_surface(X, Y, self._Q)
        ax.set_xlabel('Action')
        ax.set_ylabel('State')
        ax.set_zlabel('Expected reward')
        """