import numpy as np

class Stimulus:
    # the stimulus is the combination of the presented sample together with the recommendation
    
    SIGNAL = "signal"
    NOISE = "noise"
    
    def __init__(self, stdev, task_class, recommendation, difficulty_mapping_functions, difficulty):
        self._stdev = stdev
        self._task_class = task_class
        self._recommendation = recommendation
        self._difficulty_mapping_functions = difficulty_mapping_functions
        self._set_standard_value(difficulty)
    
    def _set_standard_value(self, target_difficulty):
        difficulty = None
        while (difficulty != target_difficulty):
            self._standard_value = np.random.normal(0, self._stdev, 1)[0]
            output = self.get_difficulty()
            if output:
                difficulty = output["label"]
    
    def get_difficulty(self):
        for difficulty_level, difficulty_item in self._difficulty_mapping_functions.items():
            outcome = difficulty_item["mapping_function"](self)
            if outcome == True:
                return {
                    "difficulty_level": difficulty_level,
                    "label": difficulty_item["label"]
                }
        
        return None
        
    def is_signal_ai(self): # if the recommendation is correct (signal) or incorrect (noise)
        return self._task_class == self._recommendation
        
    def is_signal_task(self): # if the xray sample present the signal (pneumonia) or not
        return self._task_class == Stimulus.SIGNAL
    
    def get_recommendation(self):
        return self._recommendation
    
    def get_task_outcome(self, ps_choice):
        if self.is_signal_task():
            if ps_choice == Stimulus.SIGNAL:
                return "hit"
            else:
                return "miss"
        else:
            if ps_choice == Stimulus.NOISE:
                return "correct_rejection"
            else:
                return "false_alarm"
    
    def get_ai_outcome(self, ps_choice):
        if self.is_signal_ai():
            if ps_choice == self._recommendation:
                return "hit"
            else:
                return "miss"
        else:
            if ps_choice != self._recommendation:
                return "correct_rejection"
            else:
                return "false_alarm"
    
    def get_standard_value(self):
        return self._standard_value
    
    def get_perceived_value(self, dprime):
        return self._standard_value + dprime/2 if self.is_signal_ai() else self._standard_value - dprime/2
    
    